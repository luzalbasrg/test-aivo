<?php
/**
 * author: 
 * Name: Luzalba Rincon
 * email: luzalbasrg@gmail.com
 * Date: 03/03/2018
 * Name Project: Test AIVO
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


//importando autload de slim y facebook
require '../vendor/autoload.php';
require '../src/Facebook/autoload.php';

//instanciando variable slim
$app = new \Slim\App;

$app->group('/profile/', function () {
    
    $this->get('facebook/{id}', function ($req, $res, $args) {

        
		$app_id = '150780795587432';
		$appi_secret = '0b35b31722011b2893585f9417eb4fb8';
		$autotoken = 'EAACJImPCi2gBAK73WluxsZAKodlTw6mRmxVRtkJxZCwXXFMW2AXB9Tv4WIbvvG1G3DR5IQbJtD5iazBV9MYltsKc6byWisI6dL5UYTffb1toaVhgp2KhBr4xQJpbEZAtGAZCc8ggip3S9byTblsON7bCjxdmvuCzU6kvp6yCEQZDZD';	

		//Instanciando objeto de facebook
		$fb = new Facebook\Facebook([
		  'app_id' => $app_id ,
		  'app_secret' => $appi_secret,
		  'default_graph_version' => 'v2.12',
		 ]);

		//Parámetro de la petición por HTTP
		$user_id = $args['id'];


	
		try {
		  $response = $fb->get(
		    '/'.$user_id.'?fields= id,name,first_name,last_name',$autotoken		    
		  );
		} catch(FacebookExceptionsFacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(FacebookExceptionsFacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		$user = $response->getGraphNode();

		//Informacion del Usuario
		$objetofb = array(
	        'id'=> $user['id'],
	        'firtsname'=> $user['first_name'],
	        'lastName'=> $user['last_name'],
        );

	   
	    echo json_encode($objetofb);
    });
    
});


$app->run();